---
marp: true
title: George Orwell
description: Hodně stručná prezentace na George Orwella.
lang: cs
url: https://joedmin.cz/orwell/
author: Joedmin
paginate: true
class: invert
theme: uncover
auto-scaling: true
size: 16:9
headingDivider: [1, 2, 3, 4, 5, 6]
backgroundColor: #0D1117
style: |
  h1 {
    font-size: 1.5rem;
    text-align: right;
    word-wrap: normal;
  }

  td {
    text-align: start;
  }
---

# George<br/>Orwell
<!--
_paginate: skip
-->
![bg right:60% cover](./images/cover.jpg)

## Základní informace
- Vlastním jménem Eric Arthur Blair
- 25\. června 1903 - 21. ledna 1950
- Bristský novinář, esejista a romanopisec

## Život
- 25\. června 1903, Bengálsko, Indie
- Okolo jednoho roku se s matkou a 2 sestrami stěhuje do Oxfordshiru, Anglie
- Vystudoval soukromou střední a prestižní Eton College <!-- na soukromé byl protože si jeho matka nemohla platit všemožné poplatky<br/>proti soukromým středním školám následně brojil -->
![bg right:35% cover](./images/Oxfordshir.webp)

---
- Na univerzitu si vydělával v Indické imperiální policii v (1922)
  - Slouží do svých 25 let
  - Počátky nenávisti k imperialismu
  - Naklání se k politické levici
![bg right:45% contain](./images/police.png)

## Začátky literatury
- Řeka Orwell - pseudonym
- 1927 návrat do Anglie
  - Novinář + esejista
- Žije jako tulák na ulici, okolo spodiny společnosti
  - 1933 Na dně v Paříži a Londýně
- 1934 Bármské dny
  - Zážitky z imperiální policie
- Evropské události<br/>-> socialista, antifašista a kritik nedemokracie

## Španělská občanská válka
- 1936
- Dobrovolním ve vládní straně milicí POUM <!-- Dělnické strany marxistického sjednocení -->
- Hold Katalánsku
  - 1938
  - Zážitky z války
![bg right:45% contain](./images/spanel.jpg)

## Druhá světová válka
- Díky vleklé tuberkulóze se neúčastní
- Do 1940 pro Nový anglický týdeník
- Od 1940 přispívá do novin a časopisů pro BBC, 1943 končí

## Farma zířat
- 1945
- Alegorická satirizující novela
- Politické vztahy a děje mezi lidmi formou bajky
> All animals are equal, but some animals are more equal than others
![bg left:45% contain 70%](./images/animal_farm.jpg)

## 1984
- Vydáno 1949
- Dystopický román
- Svět ovládaný absolutní totalitou živenou permanentní "udržovací" válkou
> Válka je mír, <!-- Lidi se bojí o svůj život a nemají čas myslet nad krásnými věcmi a sebou samým.  -->
> Svoboda je otroctví, <!-- Svoboda je nebezpečná pro stabilitu systému a jedinců -->
> Nevědomost je síla <!-- Je jednodušší někoho ovládat, když nic neví -->
![bg left:45% contain 70%](./images/1984.jpg)

## Prostor na dotazy
<!-- _paginate: skip -->

## Děkuji za pozornost
<!-- _paginate: skip -->
