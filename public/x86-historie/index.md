---
marp: true
title: Historie x86 Architektury
description: Hodně stručná prezentace na historii architektury x86.
lang: cs
url: https://joedmin.cz/x86-historie/
author: Joedmin
paginate: true
class: invert
theme: uncover
auto-scaling: true
size: 16:9
headingDivider: [1, 2, 3, 4, 5, 6]
backgroundColor: #0D1117
style: |
  h1 {
    font-size: 1.5rem;
    text-align: right;
    word-wrap: normal;
  }

  td {
    text-align: start;
  }
---

# Historie<br/>x86<br/>Architektury
<!--
_paginate: skip
-->
![bg right:60% cover](./images/cpu_close_up.jpg)

## Disclaimer

Prezentace může obsahovat chybné informace anebo v ní naopak některé můžou chybět.

Některé dodatečné informace můžou být nalezené v poznámkách.

## Co je x86?
- Rodina komplexní instrukční sady
  - Také jako 80x86 nebo rodina 8086
- Definuje počítačovou platformu <!-- Někde můžete číst, že je to také IBM kompatibilní, ale pozor, není! IBM kompatibilní ještě zahrnuje další specifikace na HW -->
- Nejrozšířěnější architektura *(nejen)* pro osobní počítače

## Historický background
- 1947 - Plně funkční tranzistor (Bell&nbsp;Labs)
- 1965 - Moorův zákon
- 1968 - Založen **INT**egrated&nbsp;**EL**ectionics <!-- Vyrábí SRAM -->
- 1969 - Založeno **A**dvanced&nbsp;**M**icro&nbsp;**D**evices <!-- Vyrábí registry a mikročipy a RAM -->
- 1971 - První MCU Intel&nbsp;4004 <!-- Původně pro kalkulačku, ale Intel ho propaguje jako programovatelný počítač v čipu -->
<!-- - 1969 - Na trh vstupují s SRAM 1101 (polovodíková statická)
  - Začátek konce magnetických RAM -->
![bg left:30% cover](./images/tranzistor.jpg)

---
![bg contain](./images/Moores_Law_1971-2018.png)
<!-- _footer: Ve zkratce: počet tranzistorů v integrovaných obvodech se cca každé 2 roky zdvojnásobí -->

## Intel 4004
<!-- Adresa skrze multiplexor -->
|                        |         |
| ---------------------- | ------- |
| Šířka datové sběrnice  | 4 bitů  |
| Šířka adresní sběrnice | 12 bitů |
| Paměť programu         | 4 KiB   |
| Počet tranzistrů       | 2 300   |
| Frekvence              | 740 KHz |
| Patice                 | 16 pinů |
| Výrobní proces         | 10 µm   |

![bg right:30% contain](./images/Intel_4004.jpg)

## Jak to vše začalo?

## 1972
<!-- Psal se rok 1972 a Intel přišel s novinkou - Intel 8008 -->

### Intel&nbsp;8008
|                        |               |
| ---------------------- | ------------- |
| Šířka datové sběrnice  | 8 bitů        |
| Šířka adresní sběrnice | 16 bitů       |
| Paměť programu         | 16 KiB        |
| Počet tranzistrů       | 3 500         |
| Frekvence              | 500 - 800 kHz |
| Patice                 | 18 pinů       |
| Výrobní proces         | 10 µm         |

---
![bg cover](./images/Intel_8008.jpg)

---
Jenže zákazníci se moc nehrnou <!-- Pomalý, málo pinů a nízký stack takže softwarové využití bylo hodně omezené. Intel musel přijít s něčím novým. -->

## 1974
<!-- Takže o o 2 roky později přichází s...  -->

### Intel&nbsp;8080
Specificky navržen jako univerzální MCU
<!-- Tady se už posouváme do MHZ! -->

Binárně nekompatibilní s&nbsp;8008&nbsp;-&nbsp;transpiler <!-- Transpiler - přeladač na překlad zdrojového kódu do jiného proramovacího jazyka -->

Úspěch

---
|                        |               |
| ---------------------- | ------------- |
| Šířka datové sběrnice  | 8 bitů        |
| Šířka adresní sběrnice | 16 bitů       |
| Paměť programu         | 16 KiB        |
| Počet tranzistrů       | 4 500 / 6 000 |
| Frekvence              | 2 - 3.125 MHz |
| Patice                 | 40 pinů       |
| Výrobní proces         | 6 µm          |

---
![bg cover](./images/Intel_8080.jpg)

## Rok 1975 💾
Začátek revoluce osobních počítačů

<!-- Přichází -->
### První osobní počítač
<style scoped>
  footer {
    text-align: end;
  }
</style>
- Altair&nbsp;8800 ($439) <!-- sada, že si to složíte a $621 za už složený -->
  - Poháněn Intel&nbsp;8080
  - Altair Basic (Gates&nbsp;+&nbsp;Allen) <!-- První programovací jazyk na něj vytvořek Později se z něj stane Microsoft Basic -->
- AMD a jejich Am9080 <!-- První MCU od AMD, nelincencovaný klon Intel 8080. Vyrobem zapomocí reverse-engineeringu O rok později spolu obě společnosti uzavírají dohodu a z AMD se stává licencovaný druhý dodavatel 8080 -->

![bg contain right:50%](./images/Altair_8800.jpg)
<!-- _footer: Altair 8800 s 8 palcovou disketovou mechanikou -->

<!-- Každopádně po tom Intel dostává nějaké ty rány: -->
## Rány pro Intel
<style scoped>
  ul {
    align-self: center;
  }
</style>
- Roku 1977 Apple II s 8bit&nbsp;MOS&nbsp;Technology&nbsp;8502
- Apple a Comodore u konkurenčního MOS&nbsp;Technology

![bg left:40% cover](./images/apple2_ad.jpg)

## 🚀 Vzestup x86

### 1978
- Vychází 16bit Intel 8086
  - Stane se z něj průmyslový standard
- Samostatný matematický koprocesor Intel&nbsp;8087
- x86-16 architektura

![bg right:40% contain](./images/Intel_C8086.jpg)

---
|                        |                |
| ---------------------- | -------------- |
| Šířka datové sběrnice  | 16 bitů        |
| Šířka adresní sběrnice | 20 bitů        |
| Paměť programu         | externě až 1MB |
| Počet tranzistrů       | 29 000         |
| Frekvence              | 5 - 10 MHz     |
| Patice                 | 40 pinů        |
| Výrobní proces         | 3 µm           |

<!-- A rok na to přichází Intel s -->
### 1979
- Intel 8088
  - levnější alternativa

![bg right:40% contain](./images/Intel_TD8088.jpg)

---
|                        |              |
| ---------------------- | ------------ |
| Šířka datové sběrnice  | 8 bitů       |
| Šířka adresní sběrnice | 20 bitů      |
| Paměť programu         | 16 KiB       |
| Počet tranzistrů       | 29 000       |
| Frekvence              | 5 - 10 MHz   |
| Patice                 | 40 - 44 pinů |
| Výrobní proces         | 3 µm         |

### Odstartování dominance Intelu
<!-- Ob tyhle procesory odstartovaly doinanci Intelu -->

## Výhry s IBM 🏆
- 1981 - Intel&nbsp;8088 pohání IMB&nbsp;PC
  > The biggest win ever for Intel.
  \- Intel Executive
- 1982 - AMD druhý dodavatel 8086 a 8088 pro IBM <!-- Intel "donucen" IMB - nechtěl ztratit zákazníka -->

![bg right:40% contain](./images/IBM_PC.png)

### Intel 80286
<!-- Téhož roku (1982) vychází -->
- První 16bit x86-16 s **M**emory **M**anagement **U**nit
  - OS ho používá na izolaci paměti programů

![bg right:30% contain](./images/KL_Intel_i286.jpg)

---
|                        |            |
| ---------------------- | ---------- |
| Šířka datové sběrnice  | 16 bitů    |
| Šířka adresní sběrnice | 24 bitů    |
| Paměť programu         | 16 MB      |
| Počet tranzistrů       | 134 000    |
| Frekvence              | 4 - 42 MHz |
| Patice                 | 68 pinů    |
| Výrobní proces         | 1.5 µm     |

<!-- Kromě toho je i286 první procesor podporující takzvaný... -->
### Protected mode
<!-- Nebo také -->
Virtual-address mode

#### Co to je?

#### Protected mode
- Umožňuje zajistit izolaci paměti jednotlivých programů
  - Programy si navzájem nemůžou sahat do paměti <!-- a obecně mimo svůj vymezený prostor, takže i do paměti kernelu, ... -->
- Režim vylepšen v i386 a následně v x86-64
- Každý program běží s danými oprávněnmi

#### Úrovně oprávnění
<style scoped>
  img {
    background-color: white;
    border-radius: 1rem;
  }
</style>
![](./images/Priv_rings.svg)

<!-- Spolu s tím s i286 přichází... -->
#### Virtuální paměť
- Umožňuje programu dát paměť, která není v RAM, ale např. na disku
- Může být větší než nainstalovaná RAM
- Fyzická vs virtuální adresa paměti
  - Rozlišuje sám procesor
![bg right:30% contain](./images/Virtual_memory.jpg)

## 1984
- (doslova)
- IBM PC-AT s PC-DOS <!-- Do 1993 téměř identické s MS-DOS -->
  - poháněn Intelem 80286
  - *De facto* PC standart na téměř 10 let
- AMD Am286

![bg right:40% contain](./images/IBM_PC_AT.jpg)

## 1985
- Intel
  - opouší výrobu RAM <!-- aby se mohl plně věnovat CPU -->
  - 80386 *(později jen jako i386)*
    - x86-32bit *(aka IA-32)*
      - Kód pro x86-16bit <!-- umí spouštět -->
  - Compaq - Deskpro 386 *(1986)*
- AMD experimentace s Am29000 (RISC)

![bg right:30% contain](./images/Intel_i386DX.jpg)

### Intel i386
|                        |                 |
| ---------------------- | --------------- |
| Šířka datové sběrnice  | 32 bitů         |
| Šířka adresní sběrnice | 32 bitů         |
| Paměť programu         | externě až 4 GB |
| Počet tranzistrů       | 275 000         |
| Frekvence              | 12.5 - 40 MHz   |
| Patice                 | 132 pinů        |
| Výrobní proces         | 1.5 - 1 µm      |

<!-- Jenže co se nestane: -->
### Intel drží monopol nad i836 a neprodává licenci
-> porušení smlouvy s AMD <!-- což znamená jen jedno -->

## Intel ⚔ AMD
<style scoped>
  h2 {
    background-color: rgba(13, 17, 23, .7);
  }
</style>
![bg cover](./images/SCBench.jpg)
<!-- _footer:  Supreme Court of California -->

## 1987 📝
Snaha o urovnání sporu o i836 licenci

AMD obviňuje Intel z monopolu u Kalifornského soudu

Intel se odvolává

## i486
- 1989
- Vysoká cache na čipu
- Integrovaný matematický koprocesor na čísla s plovoucí desetinnou čárkou

![bg right:40% contain](./images/i486.jpg)

## 1991
Supreme Court of California
![bg right](./images/this-is-fine.gif)

### Aby se to zkrátilo tak
<!-- Táhne se několik let -->

## AMD s 10M$ a licencí na vlastní čipy s x86 architekturou
<style scoped>
  h2 {
    bottom: 1.5rem;
    position: fixed;
    text-align: center;
}
</style>
![bg](./images/squidward-bathing-in-money.gif)

## Konec osmdesátek
- x86 s CISC pod palbou konkurence s RISC
- Intel odpovídá s i860 na RISC
  - První čip s 1M tranzistory <!-- jedině é k nějak důležité o i386 -->

![bg right:40% contain 70%](./images/KL_Intel_i860XR.jpg)

<!-- Aby toho nebylo pro RISC málo tak Intel přichází s -->
## Pentium 💪
> RISC Killer <!-- Následník i486<br/>Množstvím drtí RISC - RISC bylo málo -> drahé, ale byly rychlejší -->
- i586
- Umí
  - Předvídat instrukce
  - Provádět instrukce v jiném pořadí
- Pokročilejší jednotka s plovoucí desetinnou čárkou <!-- Bylo tvrzeno, že v CISC je nemožné ji implementovat -->
- První superskalární x86 CPU <!-- umí provádět 2 instrukce najednou -->

![bg right:40% contain 70%](./images/pentium_logo.png)

---
|                        |                             |
| ---------------------- | --------------------------- |
| Šířka datové sběrnice  | 64 bitů **(stále 32 arch)** |
| Šířka adresní sběrnice | 32 bitů                     |
| Paměť programu         | externě až 64 GB            |
| Počet tranzistrů       | 3 100 000                   |
| Frekvence              | 150 - 200 MHz               |
| Patice                 | 132 pinů                    |
| Výrobní proces         | 1.5 - 1 µm                  |

## Konec devadesátek
- Stoupá poptávka na PC
- Intel, AMD, komplexita x86 🚀
- AMD se snaží předehnat Intel

![bg right:38% contain](./images/stonks.avif)

## 64 bit

## Itanium
<!-- HP vynalezlo EPIC, Intel to s nimi dokončil jako IA-64 a nakonec vzniklo Itanium -->
- Společně vyvíjena HP a Intelem
- 2001 - výkon je oproti RISC a CISC zklamání
  - Špatná emulace x86 aplikací a systémů
- Jedny z nejhorších Intel procesorů
<!-- Časem se pak uchytily v serverech spolu s x86-64 a PowerPC -->

![bg right:40% contain 60%](./images/Itanium_logo.png)

## AMD64
- Aka x86-64
- AMD oproti Intelu nenahrazuje x86 architekturu, ale rozšiřuje ji
- 2000 - Dostupna první úplná specifikace
- Drtí zbrusu novou Intelí sadu

![bg left:40% contain 60%](./images/AMD64_Logo.png)

---
- Umožňuje práci s vyššími čísly
- Dva provozní režimy
  - 64bit
  - Kompatibility
- Čtyřúrovňový stránkovací režim
  - Aka swap - ukládání části RAM na disk

![bg left:40% contain 60%](./images/AMD64_Logo.png)

## Více jader, více síly
- 2004 - AMD demonstruje dual-core x86 CPU
- od 2005 - První dvoujádra v prodeji

### AMD Opteron model 875 (2005)
- Integrovaný ovladač pamě
  - Umožnuje až DD3 SRDRAM
  - Eliminace northbridge na MB

![bg contain right:30%](./images/opteron.jpg)

---
|                        |            |
| ---------------------- | ---------- |
| Šířka datové sběrnice  | 64 bitů    |
| Šířka adresní sběrnice | 64 bitů    |
| Frekvence              | 2x 2.2 GHz |
| Výrobní proces         | 130 nm     |

![bg contain right:30%](./images/opteron.jpg)

### Intel Core Yonah Duo (2006)
<!-- L1 - per jadro a na instrukci -->
|                        |             |
| ---------------------- | ----------- |
| Šířka datové sběrnice  | 32 bitů     |
| Šířka adresní sběrnice | 32 bitů     |
| Frekvence              | 2x 2.33 GHz |
| Výrobní proces         | 130 nm      |
| L1 cache               | 2x 32 KB    |
| L2 cache               | sdíl. 2MB   |

![bg contain right:30%](./images/yonah.jpg)

## Snaha rozšiřovat
- 2000 - 2010
- 2006 - první Apple Mac Intelem
- Intel + AMD
  - Snaha rozšířit x86 mimo desktop
    - Konzole, mobily (dominuje ARM), IoT
  - Intelu jde hlavně o telefony

### Intel Atom
- Naprosté selhání
- Jeden z dalších nejhorších Intelích CPU

![bg left:40% contain 60%](./images/intel_phone.png)

## Herní konzole
- Asi další nejbezpečnější platforma pro x86
- První je Xbox (2001) - Pentium III
- Dlouho žádné konzole nevychází
- PowerPC přebralo
  - - PS3, XB360, Wii, Wii U
- x86 do dnes zpátky v PS a XB
  - Od PS4 a XBox One
  - Oba custom AMD x86 čip

![bg right:30% contain](./images/xbox-playstation.jpg)

### Handheld
<!-- Další kde se x86 celkem uchytila (vyjma Nintenda - ARM) -->
![bg contain left:50%](./images/steam-deck-vs-rog-ally.webp)

- Steam Deck
- ROG Ally
- Ayaneo

## Budoucnost?
- Apple komplet opustil x86 a přešel na vlastní ARM Apple Silicon
- V cloudu se šíří ARM díky nízké spotřebě

### PC World
- Pozor, nečetl jsem to
![bg right contain](./images/screenshot.png)

## Prostor na dotazy
<!-- _paginate: skip -->

## Děkuji za pozornost
<!-- _paginate: skip -->

